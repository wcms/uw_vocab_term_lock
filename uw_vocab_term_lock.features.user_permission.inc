<?php

/**
 * @file
 * uw_vocab_term_lock.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function uw_vocab_term_lock_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'create field_term_lock'.
  $permissions['create field_term_lock'] = array(
    'name' => 'create field_term_lock',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'edit field_term_lock'.
  $permissions['edit field_term_lock'] = array(
    'name' => 'edit field_term_lock',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'edit own field_term_lock'.
  $permissions['edit own field_term_lock'] = array(
    'name' => 'edit own field_term_lock',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'taxonomy tab access'.
  $permissions['taxonomy tab access'] = array(
    'name' => 'taxonomy tab access',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'uw_taxonomy_rights_access',
  );

  // Exported permission: 'view field_term_lock'.
  $permissions['view field_term_lock'] = array(
    'name' => 'view field_term_lock',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'view own field_term_lock'.
  $permissions['view own field_term_lock'] = array(
    'name' => 'view own field_term_lock',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  return $permissions;
}
