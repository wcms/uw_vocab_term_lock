<?php

/**
 * @file
 * uw_vocab_term_lock.features.menu_custom.inc
 */

/**
 * Implements hook_menu_default_menu_custom().
 */
function uw_vocab_term_lock_menu_default_menu_custom() {
  $menus = array();

  // Exported menu: menu-site-manager-vocabularies.
  $menus['menu-site-manager-vocabularies'] = array(
    'menu_name' => 'menu-site-manager-vocabularies',
    'title' => 'Vocabularies',
    'description' => 'Links to vocabularies that site managers can edit.',
    'language' => 'und',
    'i18n_mode' => 0,
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Links to vocabularies that site managers can edit.');
  t('Vocabularies');

  return $menus;
}
