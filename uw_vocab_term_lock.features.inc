<?php

/**
 * @file
 * uw_vocab_term_lock.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function uw_vocab_term_lock_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}
