<?php

/**
 * @file
 * uw_vocab_term_lock.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function uw_vocab_term_lock_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: menu-site-manager-vocabularies_audience:admin/structure/taxonomy/uwaterloo_audience.
  $menu_links['menu-site-manager-vocabularies_audience:admin/structure/taxonomy/uwaterloo_audience'] = array(
    'menu_name' => 'menu-site-manager-vocabularies',
    'link_path' => 'admin/structure/taxonomy/uwaterloo_audience',
    'router_path' => 'admin/structure/taxonomy/%',
    'link_title' => 'Audience',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-site-manager-vocabularies_audience:admin/structure/taxonomy/uwaterloo_audience',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 0,
    'language' => 'und',
    'menu_links_customized' => 0,
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Audience');

  return $menu_links;
}
